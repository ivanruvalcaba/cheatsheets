[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 03 may. 2021 14:29:11)
[//]: # (Last Modified: 06 ago 2022 21:55:09)

# Cheatsheets

![Cartoon about cheatsheets](cheatsheets.png "Cartoon about cheatsheets")

This repository contains personal-sourced cheatsheets to be used with
[cheat](https://github.com/cheat/cheat) and similar applications.

## Usage

Clone this repository in the cheat's cheatsheets root directory:

```sh
git clone https://git.disroot.org/ivanruvalcaba/cheat-cheatsheets.git personal
```

### cheat configuration file

Finally, make sure that your cheat configuration file, *conf.yml*, contains
something similar to the following:

```yaml
  # If you have personalized cheatsheets, list them last. They will take
  # precedence over the more global cheatsheets.
  - name: personal
    path: C:\Users\user\AppData\Roaming\cheat\cheatsheets\personal\cheatsheets
    tags: [ personal ]
    readonly: false
```

## Format

Cheatsheets are plain-text files that begin with an optional "front matter"
header in YAML format. The header may be used to assign "tags" to a sheet, and
to specify the sheet's syntax (bash, python, go, etc). See the *languages.yml*
file for more information on a specific language.

When possible, cheatsheets should conform to this format:

```yaml
---
syntax: bash
tags: [ vcs, development ]
---
# To stage all changes in the current directory:
git add --all

# To commit staged changes:
git commit -m <message>
```

As a guideline, it is preferred to use [docopt](http://docopt.org/) syntax when
specifying parameter placeholders. In edge-cases where that syntax may cause
confusion, it is permissible to use placeholder values (foo.txt, example.com,
etc.) as necessary.

## License

Cheatsheets are licensed under GNU Free Documentation License GFDL 1.3. See
LICENSE for the full license text.

GFDL © [Iván Ruvalcaba](mailto:ivanruvalcaba@disroot.org)
